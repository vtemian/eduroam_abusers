import re
import math
import datetime


def offset_to_datetime(offset):
    now = datetime.datetime.now()

    if offset[-1] == 'h':
        ago = datetime.timedelta(hours=int(offset[1:-1])) # get 24 from -24h

    if offset[-1] == 'm':
        ago = datetime.timedelta(minutes=int(offset[1:-1])) # get 45 from -45m

    if offset[-1] == 's':
        ago = datetime.timedelta(seconds=int(offset[1:-1])) # get 45 from -45s

    if offset[-1] == 'd':
        ago = datetime.timedelta(days=int(offset[1:-1])) # get 2 from -2d

    return (now, ago, now - ago)


def bytes2h(size):
    """
        Explain how this function should behave and what is h?
    """

    if size == 0:
        return '0B'

    size_name = ("b", "KB", "MB", "GB", "TB")
    i = int(math.floor(math.log(size, 1024)))
    size = round(size / 1024 ** i, 2)

    return '%s %s' % (size, size_name[i])


def h2bytes(size):
    """
        Explain how this function should behave and what is h?
    """

    size_name = ["b", "kb", "mb", "gb", "gb", "tb"]

    new_size = int(re.search(r'\d+', size).group())
    str_part = re.search(r'[kmgtb]+', size.lower()).group()

    power = size_name.index(str_part)
    return new_size * (1024 ** power)

