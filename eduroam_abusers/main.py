import sys
import logging
import getpass
from pprint import pprint as pp
from argparse import ArgumentParser

import keyring

from .edusearch import search_for_mac, search_for_user
from .unifi import get_abusers
from .config import APP_NAME, APP_USER


def setup_logging(verbosity):
    """
        Explain log levels values
        100     -> ?
        50 - 10 -> ?
        10      -> ?
    """

    if verbosity is not None:
        if verbosity > 4:
            log_level = 10
        else:
            log_level = 50 - verbosity * 10
    else:
        log_level = 100

    logging.basicConfig(format='%(levelname)s:%(message)s', level=log_level)


def setup_password(prompt):
    """
        Explain why do you need a password for this application
    """
    if prompt:
        keyring.set_password(APP_NAME, APP_USER, getpass.getpass())
    else:
        password = keyring.get_password(APP_NAME, APP_USER)
        if password is None:
            logging.critical("Setup a password for this application.")
            sys.exit(1)

def run():
    parser = ArgumentParser(description="Easy logs")
    parser.add_argument('--verbose', '-v', action='count', default=None,
                        help="The verbosity level. More v's means more logging")
    parser.add_argument("-P", "--password-prompt", default=False,
                        action="store_true")

    subparser = parser.add_subparsers(title="Search options", dest="subcommand")
    mac = subparser.add_parser("mac")
    auto = subparser.add_parser("auto")
    user = subparser.add_parser("user")

    mac.add_argument("-m", "--mac", required=True, type=str,
                     help="The MAC to search for")
    mac.add_argument("-d", "--days-back", required=False, type=int, default=0,
                     help="Get logs from X days ago")
    auto.add_argument("-l", "--limit", required=False, type=str, default="5GB",
                      help="Get users which have download more then X bytes")
    auto.add_argument("-d", "--days-back", required=False, type=int, default=2,
                      help="Get logs from X days ago")

    args = parser.parse_args()

    setup_logging(args.verbose)
    setup_password(args.password_prompt)

    users = []
    if args.subcommand == "mac":
        users = search_for_mac(args.days_back, args.mac)

    if args.subcommand == "user":
        users = search_for_user(args.days_back, args.user)

    if args.subcommand == "auto":
        users = [search_for_mac(args.days_back, user)
                 for user in get_abusers(args.limit)]

    # TODO: use stdout and stderrr in order to facilitate the use of this
    #       script with other ones
    pp(users)

if __name__ == '__main__':
    run()
