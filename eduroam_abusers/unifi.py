import ssl
import json
import urllib2
import logging
from cookielib import CookieJar

import keyring

from .config import APP_NAME, DATA, LOGIN_URL, DATA_URL

# TODO: describe why do you need to do this. Do you really need this?
ssl._create_default_https_context = ssl._create_unverified_context


def get_cookie():
    """
        Explain how this function should behave
    """

    logging.debug('Getting cookies')
    opener = urllib2.build_opener(urllib2.HTTPCookieProcessor(CookieJar()))

    data = DATA.copy()
    data['password'] = str(keyring.get_password(APP_NAME, "unifi"))

    try:
        response = opener.open(LOGIN_URL, str(data), timeout=5)
    except urllib2.URLError as exp:
        if exp.reason == "Bad Request":
            logging.critical("Bad password")
        if "timed out" in exp.reason:
            logging.critical("Networking issue")
        # always fail hard
        raise

    return response.headers.get('Set-Cookie')


def is_valid(user):
    """
        Check if a user has: [oui, hostname, stat].
    """

    must_fields = ['oui', 'hostname', 'stat']
    missing = [field for field in must_fields if field not in user]

    return missing


def get_abusers(limit):
    """
        Explain how this function should behave and what is h?
    """

    cookie = get_cookie()
    if not cookie:
        raise Exception("Can't retrieve cookie. Cehck your network connection")

    request = urllib2.Request(DATA_URL)
    request.add_header('Cookie', cookie)

    logging.debug("Reading from HTTP stream, please wait")
    response = urllib2.urlopen(request)

    abusers = []
    invalid_users = 0

    # TODO: check for json format
    users = json.loads(response.read())
    for user in users['data']:
        missing_fields = is_valid(user)

        if not missing_fields:
            if user['stat']['rx_bytes'] >= h2bytes(limit):
                abusers.append(user['mac'])
                logging.info("Abuser { [OUI: %s] [Hostname: %s] [MAC: %s] } uploaded size %s, downloaded size %s",
                             user['oui'], user['hostname'], user['mac'],
                             bytes2h(user['stat']['tx_bytes']),
                             bytes2h(user['stat']['rx_bytes']))
        else:
            logging.debug("Missing fields [%s] on user %s",
                          ",".join(missing_fields), user['oui'])
            invalid_users += 1

    logging.info("Total users with missing values %s", invalid_users)
    logging.info("Total user count that exceed %s limit is %s", limit, len(abusers))
    return abusers
