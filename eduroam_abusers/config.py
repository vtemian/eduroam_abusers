APP_NAME = "eduroam_abusers"
APP_USER = "unifi"
LOGSTASH_URL = "http://10.9.0.60:9200/"
LOGIN_URL = 'https://10.3.0.2:8443/api/login'
DATA_URL = 'https://10.3.0.2:8443/api/s/default/stat/alluser?pretty=true'
DATA = {
    "username": "admin",
    "password": "password",
    "strict": "true"
}
