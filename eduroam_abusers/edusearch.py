import json
import urllib2
import logging

from .utils import offset_to_datetime
from .queries import SEARCH_BY_MAC
from .config import LOGSTASH_URL


def search_for_mac(days_back, mac):
    """
        Explain how this magic function behave
    """

    now, _, offset = offset_to_datetime("-24h")

    logging.debug("Searching for MAC %s", mac)
    query_json = json.dumps(SEARCH_BY_MAC(mac, offset))

    users = {}
    for day_ago in range(days_back, -1, -1):
        logstash_url = "%slogstash-%d.%.2d.%.2d/_search?pretty=true" % \
                        (LOGSTASH_URL, now.year, now.month, now.day - day_ago)
        req = urllib2.Request(logstash_url, query_json,
                              {'Content-Type': 'application/json'})

        # fail hard in case of networking issues or implement a retry mechanism
        response = urllib2.urlopen(req, timeout=15).read()
        # TODO: check for a valid json. Maybe a different response/format was given
        # TODO: try to use erequests libs with support for concurrency
        response = json.loads(response)

        for hit in response['hits']['hits']:
            if hit['_source']['user'] not in users:
                users[hit['_source']['user']] = 1
            else:
                users[hit['_source']['user']] += 1

            logging.info("Found user %s associated at %s",
                         hit['_source']['user'], hit['_source']['ap-name'])

    return users

def search_for_user(days_back, user):

    ago = datetime.timedelta(hours=24)
    offset = (now - ago).isoformat()
    search_user = {"query":{"filtered":{"query":{"bool":{"should":[{"query_string":{"query":"*"}}]}},"filter":{"bool":{"must":[{"range":{"@timestamp":{"from":1456014984840,"to":1456187784840}}},{"fquery":{"query":{"query_string":{"query":"tags:(\"eduroam\")"}},"_cache":"true"}},{"fquery":{"query":{"query_string":{"query":"mac:(\"28:cf:e9:19:f1:57\")"}},"_cache":"true"}}]}}}},"highlight":{"fields":{},"fragment_size":2147483647,"pre_tags":["@start-highlight@"],"post_tags":["@end-highlight@"]},"size":500,"sort":[{"@timestamp":{"order":"desc","ignore_unmapped":"true"}},{"@timestamp":{"order":"desc","ignore_unmapped":"true"}}]}
    new_user = 'mac:("%s")' % user # needs fix
    search_user['query']['filtered']['filter']['bool']['must'][2]['fquery']['query']['query_string']['query'] = new_mac
    query_json = json.dumps(search_user)
    for day_ago in range(days_back, -1, -1):
        logstash_url2 = "%slogstash-%d.%.2d.%.2d/_search?pretty=true" % (logstash_url, now.year, now.month, now.day-day_ago)
        req = urllib2.Request(logstash_url2, query_json, {'Content-Type': 'application/json'})
        f = urllib2.urlopen(req)
        raw_response = f.read()
        response = json.loads(raw_response)
        users = {}
