import datetime

from .utils import offset_to_iso


def SEARCH_BY_MAC(mac, offset):
    offset = offsetisoformat()

    return {
            "query": {
                "filtered": {
                    "query": {
                        "bool": {
                            "should": [
                                {
                                    "query_string": {
                                        "query":"*"
                                    }
                                }
                            ]
                        }
                    },
                    "filter": {
                        "bool": {
                            "must": [
                                {
                                    "range": {
                                        "@timestamp": {
                                            "from": "%s+03" % (offset, ),
                                            "to":"now"
                                        }
                                    }
                                },
                                {
                                    "fquery": {
                                        "query": {
                                            "query_string": {
                                                "query": "tags:(\"eduroam\")"
                                            }
                                        },
                                        "_cache": "true"
                                    }
                                },
                                {
                                    "fquery": {
                                        "query": {
                                            "query_string": {
                                                "query": 'mac:("%s")' % mac
                                            }
                                        },
                                        "_cache": "true"
                                    }
                                }
                            ]
                        }
                    }
                }
            },
            "highlight": {
                "fields": {},
                "fragment_size": 2147483647,
                "pre_tags": ["@start-highlight@"],
                "post_tags": ["@end-highlight@"]
            },
            "size": 1000,
            "sort": [
                {
                    "@timestamp": {
                        "order": "desc",
                        "ignore_unmapped": "true"
                    }
                },
                {
                    "@timestamp": {
                        "order": "desc",
                        "ignore_unmapped": "true"
                    }
                }
            ]
        }

